# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 0.2.0 - 2021-10-15
### Added
- Set the coordinates of a point from a geo URI

### Changed
- Use Webpack to bundle and minify the JavaScript code

## 0.1.0 - 2021-10-14

This is the initial release which provides the basis - e.g. the models, admin
integration and a block. The frontend part to display a map on your site is
currently not provided, but you can find an example.
