��    %      D  5   l      @     A     [  *   m     �     �     �  N   �     "     .     ;  	   D     N     R     Z  0   b     �     �     �  �   �  W   3     �     �     �     �     �     �     �  	   �               
          0     5     ;     B    H     \     z  2   �     �  #   �     �  U        W     d     q  	   z     �     �     �  ?   �     �     �     �  �   	  ]   �	     �	     �	     
     2
     :
     C
     V
  	   h
     r
     x
     
     �
     �
     �
     �
     �
           "                #                 
                                    !                    $      %                                                                     	    Calculate from the points Center of the map Choose between a link to a page or an URL. Close Coordinates from geo URI… Coordinates of the point Enter the geo URI of your point to set its latitude and longitude accordingly. Height (px) Initial zoom Latitude Longitude Map Maximum Minimum Minimum zoom level must be smaller than maximum. Point Points Set coordinates Unable to calculate the geometric center of the points. Verify that you have at least one point with valid latitude and longitude. Unable to parse the geo URI. Verify that it is in the form of <code>geo:1.0,2.0</code>. Zoom levels center point's latitude center point's longitude content latitude link to a page link to an URL longitude map maps maximum zoom level minimum zoom level name point points title Project-Id-Version: wagtail-maps
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-15 15:36+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Calculer à partir des points Centre de la carte Choisissez entre un lien vers une page ou une URL. Fermer Coordonnées depuis une URI géo… Coordonnées du point Entrez l'URI géo du point pour définir sa latitude et sa longitude en conséquence. Hauteur (px) Zoom initial Latitude Longitude Carte Maximum Minimum Le niveau de zoom minimum doit être plus petit que le maximum. Point Points Définir les coordonnées Impossible de calculer le centre géographique des points. Vérifiez qu'au moins un point est défini avec une latitude et longitude valides. Impossible d'analyser l'URI géo. Vérifiez qu'elle est de la forme <code>geo:1.0,2.0</code>. Niveaux de zoom latitude du point central longitude du point central Contenu latitude lien vers une page lien vers une URL longitude carte cartes niveau de zoom maximum niveau de zoom minimum nom point points Titre 