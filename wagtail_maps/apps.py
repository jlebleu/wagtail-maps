from django.apps import AppConfig


class WagtailMapsConfig(AppConfig):
    name = 'wagtail_maps'
    default_auto_field = 'django.db.models.AutoField'
