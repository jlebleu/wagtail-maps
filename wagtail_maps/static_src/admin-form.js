// =============================================================================
// Constants
// =============================================================================

const RE_GEO_URI =
  /^geo:([-+]?\d+(?:\.\d+)?),([-+]?\d+(?:\.\d+)?)(?:\?z=(\d{1,2}))?$/;

const ID_CENTER_LATITUDE = 'id_center_latitude';
const ID_CENTER_LONGITUDE = 'id_center_longitude';
const ID_CENTER_CALCULATE = 'map-center-calculate';
const ID_POINT_FROM_GEO_MODAL = 'point-from-geo-modal';

const NAME_POINT_PREFIX = 'prefix';
const NAME_POINT_GEO_URI = 'geo-uri';

const SELECTOR_ERROR_MESSAGE = '.error-message';
const SELECTOR_POINTS_FORMS = '#id_points-FORMS';
const SELECTOR_POINT_FORM = `${SELECTOR_POINTS_FORMS} > li:not(.deleted)`;

// =============================================================================
// Helpers
// =============================================================================

function isNumber(value) {
  return value === Number(value).toString();
}

function add(accumulator, a) {
  return accumulator + a;
}

// =============================================================================
// Main functions
// =============================================================================

// Calculate the center of the map from the centroïd of the points.
document.addEventListener('DOMContentLoaded', () => {
  const latInput = document.getElementById(ID_CENTER_LATITUDE);
  const lngInput = document.getElementById(ID_CENTER_LONGITUDE);
  const calcButton = document.getElementById(ID_CENTER_CALCULATE);
  const errorMessage = calcButton.parentElement.querySelector(
    SELECTOR_ERROR_MESSAGE
  );

  calcButton.addEventListener('click', () => {
    const lat = [];
    const lng = [];

    errorMessage.setAttribute('hidden', '');

    // iterate over points and append its latitude and longitude
    document.querySelectorAll(SELECTOR_POINT_FORM).forEach((child) => {
      const latitude = child.querySelector('[name$=-latitude]').value;
      const longitude = child.querySelector('[name$=-longitude]').value;

      if (isNumber(latitude) && isNumber(longitude)) {
        lat.push(Number(latitude));
        lng.push(Number(longitude));
      }
    });

    if (!lat.length) {
      errorMessage.removeAttribute('hidden');
      return;
    }

    latInput.value = lat.reduce(add) / lat.length;
    lngInput.value = lng.reduce(add) / lng.length;
  });
});

// Handle the modal for getting a point's coordinates from a geo URI.
document.addEventListener('DOMContentLoaded', () => {
  const modalContainer = document.getElementById(ID_POINT_FROM_GEO_MODAL);
  const form = modalContainer.querySelector('form');
  const prefixInput = form.querySelector(`input[name="${NAME_POINT_PREFIX}"]`);
  const geoInput = form.querySelector(`input[name="${NAME_POINT_GEO_URI}"]`);
  const errorMessage = form.querySelector(SELECTOR_ERROR_MESSAGE);

  window.jQuery(modalContainer).on('show.bs.modal', (event) => {
    geoInput.value = '';
    errorMessage.setAttribute('hidden', '');

    // store the prefix of the point's form
    prefixInput.value = event.relatedTarget.getAttribute('data-point-prefix');
  });

  form.addEventListener('submit', (event) => {
    event.preventDefault();
    event.stopPropagation();

    errorMessage.setAttribute('hidden', '');

    const data = new FormData(form);
    const geoMatch = data.get(NAME_POINT_GEO_URI).match(RE_GEO_URI);

    if (!geoMatch) {
      errorMessage.removeAttribute('hidden');
      return;
    }

    const prefix = data.get(NAME_POINT_PREFIX);
    const latInput = document.querySelector(`[name="${prefix}-latitude"]`);
    const lngInput = document.querySelector(`[name="${prefix}-longitude"]`);

    latInput.value = Number(geoMatch[1]);
    lngInput.value = Number(geoMatch[2]);

    window.jQuery(modalContainer).modal('hide');
  });
});
